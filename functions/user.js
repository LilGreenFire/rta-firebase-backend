module.exports = function(express, admin) {
  var router = express.Router();

  // creates user
  // body: {username:`${username}`}
  router.post('/user', (req, res) => {
    admin.database().ref(`usernames/${req.body.username}`).once('value')
      .then(snap => {
        if(snap.val() === null) {
            admin.database().ref(`usernames/${req.body.username}`).set(req.user.uid)
              .then(snap => {
                let user = {username: req.body.username};
                admin.database().ref(`users/${req.user.uid}`).set(user)
                  .then(snap => {
                    res.sendStatus(200);
                  })
                  .catch(() => {
                    res.status(400).send("Failed");
                  });
              })
              .catch(() => {
                res.status(400).send("Failed");
              })
        } else {
          res.status(400).send("Username already exists")
        }
      })
      .catch(() => {
        res.status(400).send("Failed");
      })
  });

  // check if user exists
  router.get('/user', (req, res) => {
     admin.database().ref(`/users/${req.user.uid}`).once("value", snapshot => {
        if (snapshot.val()) {
          res.status(200).send(snapshot.val());
        } else {
          res.status(404).send(undefined);
        }
    });
  });

  // get username
  // params: { user: `${userID}` }
  router.get('/username/:user', (req, res) => {
    admin.database().ref(`users/${req.params.user}/username`).once("value", snap => {
      res.status(200).send(snap.val());
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  // get all usernames in system
  router.get('/usernames', (req, res) => {
    admin.database().ref(`/usernames`).once("value").then(snap => {
      res.status(200).send(Object.keys(snap.val()));
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  return router;
}
