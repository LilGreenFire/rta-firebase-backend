module.exports = function (express, admin) {
	var router = express.Router();

	router.get('/regression/:numFeatures/:data/:learning_rate/:max_steps', (req, res) => {
		let numFeatures = req.params.numFeatures;
		let data = req.params.data;
		let learning_rate = req.params.learning_rate;
		let max_steps = req.params.max_steps;

		info = regression(numFeatures, data, learning_rate, max_steps);

		// send back training result
		res.status(200).send(info);
	})

	return router;
}

function regression(numFeatures, dataFileName, lr, max_steps) {

	console.log(numFeatures, dataFileName);
	let tf = require('deeplearn');
	let data = require('./data/sample_run1.json');
	console.log(data);

	const xs = tf.tensor2d([
		[40.03, 9.01, 400, 40],
		[60.1, 15.99, 400, 74.5],
		[75.03, 10.94, 400, 48],
		[75.01, 11.9, 400, 54.3],
		[75, 18.06, 400, 86.3],
		[75, 13.18, 400, 61.35],
		[75.09, 16.07, 400, 75.4],
		[75.05, 14.16, 400, 65.75],
		[75.37, 14.91, 400, 70],
		[75.09, 12.25, 400, 56.2],
		[75.23, 17, 400, 80],
		[75, 13.38, 400, 61.9],
		[75, 14.45, 400, 66.7],
		[75.02, 12.56, 400, 57.7],
		[75.02, 13.75, 400, 63.8],
		[75.08, 15.5, 400, 72.2],
		[75.1, 16.5, 400, 77.8],
		[75.05, 11.71, 400, 53.2],
		[75.08, 14.2, 400, 65.9],
		[75, 15.08, 400, 70.2],
		[75.05, 11.71, 400, 53.7],
		[75.01, 15.2, 400, 71.5],
		[75.1, 16.05, 400, 74.6],
		[75.05, 13.22, 400, 60.8],
		[75, 12.53, 400, 57.3],
		[75, 14.36, 400, 66.5],
		[75, 14.75, 400, 68.2],
		[75, 12.53, 400, 56.7],
		[75.02, 12.53, 400, 57.1],
		[75.03, 15.74, 400, 73],
		[75, 14.66, 400, 68],
		[75.03, 14.24, 400, 65.8],
		[75, 14.2, 400, 65.2]
	]);
	const ys = tf.tensor1d([
		29.5,
		81.75,
		38.6,
		47.85,
		105.9,
		59.975,
		85.5,
		67.65,
		75.83,
		53.47,
		97.55,
		65.65,
		78,
		56.9,
		70.5,
		86.9,
		95.85,
		51.3125,
		75.39583333,
		83.25,
		51.91666667,
		88.54166667,
		100,
		72.2,
		63.125,
		78.33333333,
		79.8,
		56.375,
		64.20833333,
		98.89583333,
		77.25,
		81.925,
		80.16666667
	]);

	// standardize data
	let result = tf.moments(xs);
	let xs_stand = tf.div(xs.sub(result.mean), result.variance);

	// initilize trainable weight and bias
	w = tf.variable(tf.randomNormal([4, 1], stddev = 1));
	b = tf.scalar(Math.random()).variable();

	// linear model
	const f = x => x.matMul(w).add(b);
	const loss = (y, x) => y.sub(x).square().mean();

	// set hyper-parameters and optimizer
	const learning_rate = 0.00001;
	const optimizer = tf.train.sgd(learning_rate);

	// run training loop
	const MAXSTEPS = 100;
	for (let i = 0; i < MAXSTEPS; i++) {
		optimizer.minimize(() => loss(f(xs_stand), xs_stand));
	}

	// organize training results
	flattened_data = w.dataSync();
	let w_display = [];
	for (let i = 0; i < numFeatures; i++) {
		w_display.push(w_display[i]);
	}

	// return training results
	return {
		w,
		w_display,
		b
	};

};