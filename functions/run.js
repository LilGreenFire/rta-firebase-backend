module.exports = function(express, admin) {
  var router = express.Router();
  let helpers = require("./helpers")(admin);

  var bucket = admin.storage().bucket();
  var fs = require('fs');
  var os = require('os');
  var path = require('path');
  let dir = os.tmpdir();

  // create a run by storing metadata in firebase and the run data in a file in firebase storage
  // params: { team: `${teamID}`, project: `${projectID}` }
  // body: { run: `${runData}` }
  // runData format: [
  //  {time: `${timestamp}`, sensor1: `${sensor1Data}, sensor2: `${sensor2Data}},
  //  {time: `${timestamp}`, sensor1: `${sensor1Data}, sensor2: `${sensor2Data}}
  // ]
  router.post('/team/:team/project/:project/run', (req, res) => {
    // helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      let senses = [];
      for(let i = 0; i < req.body.run.length; i++) {
        for(let key in req.body.run[i]) {
          if(key !== 'time' && senses.indexOf(key) === -1) {
            senses.push(key);
          }
        }
      }
      admin.database().ref(`runs`).push(senses).then(snap => {
        let runId = snap.key;
        let projRunId;
        admin.database().ref(`projects/${req.params.project}/runs`).push(runId).then(snap => {
          projRunId = snap.key;

          let jsonRun = JSON.stringify(req.body.run);
          let tempFilePath = path.join(dir, `${runId}.json`);
          fs.writeFileSync(`${dir}/${runId}.json`, jsonRun, (err) => {
            if(err) { 
              admin.database().ref(`runs/${runId}`).remove();
              admin.database().ref(`projects/${req.params.project}/runs/${projRunId}`).remove();
              fs.unlinkSync(tempFilePath);
              res.sendStatus(400);
            } 
          });

          bucket.upload(`${dir}/${runId}.json`, {
            destination: `runs/${runId}.json`
          }, (err, file) => {
            if(err) {
              admin.database().ref(`runs/${runId}`).remove();
              admin.database().ref(`projects/${req.params.project}/runs/${projRunId}`).remove();
              fs.unlinkSync(tempFilePath);
            } else {
              fs.unlinkSync(tempFilePath);
              res.sendStatus(200);
            }
          })
        }).catch(err => {
          admin.database().ref(`runs/${runId}`).remove();
          admin.database().ref(`projects/${req.params.project}/runs/${projRunId}`).remove();
          fs.unlinkSync(tempFilePath);
          res.sendStatus(400);
        })
      }).catch(err => {
        res.sendStatus(400);
      })
    // }).catch(err => {
    //   res.status(400).send(err);
    // });
  })

  // gets a list of runs with the sensors/senses used
  // params: { team: `${teamID}`, project: `${projectID}` }
  router.get('/team/:team/project/:project/runs', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      let runs = [];
      let promises = [];
      admin.database().ref(`projects/${req.params.project}/runs`).once('value').then(snap => {
        snap.forEach((run) => {
          let p = admin.database().ref(`runs/${run.val()}`).once('value').then(snap => {
            let obj = {};
            obj[snap.key] = snap.val();
            runs.push(obj);
          }).catch(err => {
            res.sendStatus(400);
          })
          promises.push(p);
        })
        Promise.all(promises).then(() => { res.status(200).send(runs); });
      }).catch(err => {
        res.sendStatus(400);
      })
    }).catch(err => {
      res.status(400).send(err);
    })
  })

  // gets a single run data
  // params: { team: `${teamID}`, project: `${projectID}`, run: `${runID}` }
  router.get('/team/:team/project/:project/run/:run', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      let tempFilePath = path.join(dir, `${req.params.run}.json`);
      bucket.file(`runs/${req.params.run}.json`).download({
        destination: tempFilePath
      }).then(() => {
        let contents = fs.readFileSync(tempFilePath);
        var json = JSON.parse(contents);
        res.status(200).send(json);
        fs.unlinkSync(tempFilePath);
      }).catch(err => {
        res.sendStatus(400);
      })
    }).catch(err => {
      res.status(400).send(err);
    })
  })

  return router;
}