// module.exports = function(express, admin) {
//   const Rx = require('rxjs');

//   var router = express.Router();

//   var ref = admin.database().ref('rxjs-demo');

//   const evenNumbers = Rx.Observable.create((observer) => {
//     let value = 0;
//     const interval = setInterval(() => {
//       if (value % 2 === 0) {
//         observer.next(value);
//       }
//       value++;
//     }, 1000);

//     return () => clearInterval(interval);
//   });

//   const subject = new Rx.Subject();

//   // const subscription = subject.subscribe(
//   //   function (x) {
//   //       console.log('Next: ' + x.toString());
//   //   },
//   //   function (err) {
//   //       console.log('Error: ' + err);
//   //   },
//   //   function () {
//   //       console.log('Completed');
//   //   });

//   router.get(`/observe`, (req, res) => {
//     let value = 1;
//     setInterval(() => {
//       console.log("value", value);
//       if (value % 2 === 0) {
//         subject.next(value);
//       }
//       if(value % 20 === 0) {
//         subject.complete();
//       }
//       value++;
//     }, 1000);
//     res.status(200).send(subject);
//   });

//   return router;
// }