const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
console.log(functions.config());
const config = functions.config();
const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});
const atob = require('atob');
const app = express();


const team = require("./team")(express, admin);
const project = require("./project")(express, admin);
const device = require("./device")(express, admin, config, functions);
const user = require("./user")(express, admin);
const sense = require("./sense")(express, admin);
const run = require("./run")(express, admin);
const regression = require("./regression")(express, admin);

console.log("config");
console.log(functions.config().firebase.credential);

const validateFirebaseIdToken = (req, res, next) => {
  if((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) && !req.cookies.__session) {
    res.status(403).send('Unauthorized');
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else {
    idToken = req.cookies.__session;
  }
  admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
    req.user = decodedIdToken;
    next();
  }).catch(error => {
    res.status(403).send('Unauthorized');
  });
};



app.use(cors);
app.use(cookieParser);

app.use(validateFirebaseIdToken);

app.use(user);
app.use(team);
app.use(project);
app.use(device);
app.use(sense);
app.use(run);
app.use(regression);

exports.app = functions.https.onRequest(app);


exports.sensorPubSub = functions.pubsub.topic('sensordata').onPublish(event => {
  admin.database().ref(`/devices/${event.data.json.deviceId}/sensors`).set(event.data.json.sensors);
  return 1;
});