module.exports = function(express, admin) {
  var router = express.Router();
  let helpers = require("./helpers")(admin);

  // gets all senses for a project
  // params: {team: `${teamID}`, project: `${projectID}`}
  // returns: status 200, array of senses
  router.get('/team/:team/project/:project/senses', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      admin.database().ref(`projects/${req.params.project}/senses`).once('value').then(snap => {
        let senses = [];
        let promises = [];
        snap.forEach(sense => {
          let p = admin.database().ref(`senses/${sense.val()}`).once('value').then(snap => {
            let sense = {};
            sense[snap.key] = snap.val()
            senses.push(sense);
          }).catch(err => {
            res.status(400).send('Could not fetch one of your senses');
          })
          promises.push(p);
        })
        Promise.all(promises).then(() => res.status(200).send(senses));
      })
    }).catch(err => {
      res.status(400).send(err);
    });
  });

  // get a sense for a project
  // params: {team: `${teamID}`, project: `${projectID}`, sense: `${senseID}`}
  // returns: status 200, a sense object
  router.get('/team/:team/project/:project/sense/:sense', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      admin.database().ref(`senses/${req.params.sense}`).once('value').then(snap => {
        res.status(200).send(snap.val());
      }).catch(err => {
        res.status(400).send('Could not fetch sense');
      })
    }).catch(err => {
      res.status(400).send(err);
    });
  });

  // creates a sense for a project
  // params: {team: `${teamID}`, project: `${projectID}`}
  // body: {name: `${senseName}`, default: `${defaultValue}`, timing: `${'before', 'after', or 'both'}`, unit: `${senseUnit}`}
  // returns: status 200, id of sense
  router.post('/team/:team/project/:project/sense', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      if(['before', 'after', 'both'].indexOf(req.body.timing) === -1) {
        res.status(400).send("timing sent in body must have value 'before', 'after', or 'both'");
      }
      if(!req.body.name || !req.body.default || !req.body.unit) {
        res.status(400).send('body requires a name, unit, and default value');
      }

      admin.database().ref('senses').push(req.body).then(snap => {
        let id = snap.key;
        admin.database().ref(`projects/${req.params.project}/senses`).push(id).then(snap => {
          res.status(200).send(id);
        }).catch(() => {
          res.status(400).send('Unable to add sense to project');
        });
      }).catch(() => {
        res.status(400).send('Unable to create sense');
      });
    }).catch(err => {
      res.status(400).send(err);
    });
  });

  // edits a sense
  // params: {team: `${teamID}`, project: `${projectID}`, sense: `${senseID}`}
  // body: {
  //        (optional) name: `${senseName}`, 
  //        (optional) default: `${defaultValue}`, 
  //        (optional) timing: `${'before', 'after', or 'both'}`, 
  //        (optional) unit: `${senseUnit}`
  //       }
  router.put('/team/:team/project/:project/sense/:sense', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      let promises = [];
      if(req.body.name) {
        let p = admin.database().ref(`senses/${req.params.sense}/name`).set(req.body.name).then().catch(() => { res.sendStatus(400) });
        promises.push(p);
      }
      if(req.body.default) {
        let p = admin.database().ref(`senses/${req.params.sense}/default`).set(req.body.default).then().catch(() => { res.sendStatus(400) });
        promises.push(p);
      }
      if(req.body.unit) {
        let p = admin.database().ref(`senses/${req.params.sense}/unit`).set(req.body.unit).then().catch(() => { res.sendStatus(400) });
        promises.push(p);
      }
      if(req.body.timing) {
        if(['before', 'after', 'both'].indexOf(req.body.timing) === -1) {
          res.status(400).send("timing sent in body must have value 'before', 'after', or 'both'");
        } else {
          let p = admin.database().ref(`senses/${req.params.sense}/timing`).set(req.body.timing).then().catch(() => { res.sendStatus(400) });
          promises.push(p);
        }
      }
      Promise.all(promises).then(() => res.sendStatus(200));
    }).catch(err => {
      res.status(400).send(err);
    })
  })

  // remove sense
  // params: {team: `${teamID}`, project: `${projectID}`, sense: `${senseID}`}
  router.delete('/team/:team/project/:project/sense/:sense', (req, res) => {
    helpers.isOnTeam(req.params.team, req.user.uid).then(() => {
      admin.database().ref(`projects/${req.params.project}/senses`).once('value')
        .then(snap => {
          snap.forEach(sense => {
            if(sense.val() === req.params.sense) {
               admin.database().ref(`projects/${req.params.project}/senses/${sense.key}`).remove();
            }
          })
          admin.database().ref(`senses/${req.params.sense}`).remove();
          res.status(200).send('deleted');
        })
        .catch(() => {
          res.status(400).send("Bad request. Fail to delete sense!!");
        });
    }).catch(err => {
      res.status(400).send(err);
    })
  }); 

  return router;
}
