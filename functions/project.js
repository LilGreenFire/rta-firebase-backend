module.exports = function(express, admin) {
  var router = express.Router();
  let helpers = require("./helpers")(admin);

  // gets all project in a team
  // params: {team: `${teamID}`}
  router.get('/team/:team/project', (req, res) => {
    admin.database().ref(`teams/${req.params.team}/projects`).once('value').then(snap => {
      let projects = [];
      let promises = [];
      snap.forEach((projectID) => {
        let p = admin.database().ref(`projects/${projectID.val()}`).once('value').then(snap => {
          let project = {};
          project[snap.key] = snap.val();
          projects.push(project);
        });
        promises.push(p);
      });
      Promise.all(promises).then(() => res.status(200).send(projects));
    });
  });

  // get a single project
  // params {team: `${teamID}`, project: `${projectID}`}
  router.get('/team/:team/project/:project', (req, res) => {
    admin.database().ref(`projects/${req.params.project}`).once('value').then(snap => {
      res.status(200).send(snap.val());
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  // creates project
  // params: {team: `${teamID}`}
  // body: {name:`${pName}`}
  router.post('/team/:team/project', (req, res) => {
    helpers.isTeamAdmin(req.params.team, req.user.uid).then(username => {
      let project = {name: req.body.name, 
                      runs:{}, 
                      variables:{}, 
                      predictions:{}, 
                      sensors:{}, 
                      devices:{}};
      admin.database().ref('projects').push(project).then(snap => {
        let id = snap.key;
        admin.database().ref(`teams/${req.params.team}/projects`).push(id);
        res.status(200).send(snap.key);
      });
    }).catch(err => {
      res.status(400).send(err);
    })
  });

  // remove project
  // params: {team: `${teamID}`, project: `${projectID}`}
  router.delete('/team/:team/project/:project', (req, res) => {
    helpers.isTeamAdmin(req.params.team, req.user.uid).then(username => {
      admin.database().ref(`teams/${req.params.team}/projects`).once('value')
        .then(snap => {
          for(var key in snap.val()) {
            if (snap.val()[key] == req.params.project) {
              admin.database().ref(`teams/${req.params.team}/projects/${key}`).remove();
            }
          }
          admin.database().ref(`projects/${req.params.project}`).remove();
          res.status(200).send('deleted');
        })
        .catch(() => {
          res.status(400).send("Bad request. Fail to delete project!!");
        });
    }).catch(err => {
      res.status(400).send(err);
    })
  }); 

  return router;
}
