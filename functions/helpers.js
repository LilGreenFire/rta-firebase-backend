module.exports = function(admin) {
  let module = {};
  
  // checks if member with uid is an admin of given team
  // resolves with username, or rejects with message
  module.isTeamAdmin = function(teamId, uid) {
    return new Promise((resolve, reject) => {
      admin.database().ref(`users/${uid}/username`).once('value').then(snap => {
        let username = snap.val();
        admin.database().ref(`teams/${teamId}/members/${username}`).once('value').then(snap => {
          if(snap.val() === "admin") {
            resolve(username);
          } else {
            reject("User is not admin");
          }
        }).catch(() => {
          reject("Team does not exist");
        })
      }).catch((err) => {
        reject("User doesn't exist");
      })
    })
  }

  // checks if member with uid is a user or admin of given team
  // resolves with true, or rejects with message
  module.isOnTeam = function(teamId, uid) {
    return new Promise((resolve, reject) => {
      admin.database().ref(`users/${uid}/username`).once('value').then(snap => {
        let username = snap.val();
        admin.database().ref(`teams/${teamId}/members/${username}`).once('value').then(snap => {
          if(snap.val()) {
            resolve(username);
          } else {
            reject("User is not part of team");
          }
        })
      })
    })
  }

  module.getUser = function(username) {
    return new Promise((resolve, reject) => {
      admin.database().ref(`usernames/${username}`).once('value').then(snap => {
        admin.database().ref(`users/${snap.val()}`).once('value').then(snap => {
          let user = {};
          user[snap.key] = snap.val();
          resolve(user);
        }).catch(() => {
          reject(`User ${snap.val()} doesn't exist`);
        })
      }).catch(() => {
        reject(`Username ${username} doesn't exist`);
      });
    })
  }

  return module;
}
