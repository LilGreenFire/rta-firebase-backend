module.exports = function(express, admin, config, functions) {
  const jwt = require('jsonwebtoken');
  const rpn = require('request-promise-native');

  var router = express.Router();

  const gcp_url = "https://cloudiot.googleapis.com/v1/projects/realtimesense/locations/us-central1/registries/rtadevices/devices";

  function createJwt() {
    const token = {
      'iss': `${config.service_account.client_email}`,
      'scope': 'https://www.googleapis.com/auth/cloud-platform https://www.googleapis.com/auth/cloudiot',
      'iat': parseInt(Date.now() / 1000),
      'exp': parseInt(Date.now() / 1000) + 60 * 60,  // 60 minutes (max)
      'aud': 'https://www.googleapis.com/oauth2/v4/token'
    };
    let privateKey = config.service_account.private_key;
    privateKey = privateKey.replace(/\\n/g, "\n"); // private key in environment has \n that isn't interpreted

    return jwt.sign(token, privateKey, { algorithm: "RS256" });
  }

  function obtainAccessToken() {
    let token = createJwt();
    let grant_type = "urn:ietf:params:oauth:grant-type:jwt-bearer";
    let options = {
      method: "POST",
      url: 'https://www.googleapis.com/oauth2/v4/token',
      form: {
        grant_type: grant_type, 
        assertion: token
      },
      json: true
    }
    let req = rpn(options);

    return req;
  }

  createDeviceObject = function(name, key) {
    let device = {};
    device.credentials = [];
    device.credentials.push(
      {
        "publicKey" : {
          "format" : "RSA_X509_PEM",
          "key" : key
        },
        "expirationTime" : "1970-01-01T00:00:00Z"
      }
    )
    device.id = `${name}`;
    
    return device;
  }

  // create a device
  // params: { team: `${teamID}`, project: `${projectID}` }
  // body: { id: `${uniqueDeviceName}`, key: `${publicKey}`, name: `${deviceName}` }
  router.post(`/team/:team/project/:project/device`, (req, res) => {
    obtainAccessToken().then(access_token_resp => {
      let access_token = access_token_resp.access_token;
      let device_id = req.body.id;
      let device = createDeviceObject(device_id, req.body.key);
      let options = {
          method: "POST",
          url: gcp_url,
          headers: { Authorization: `Bearer ${access_token}` },
          body: device,
          json: true
        }
      rpn(options).then(response => {
        admin.database().ref(`projects/${req.params.project}/devices`).push(device_id).then(snap => {
          let firebaseDevice = { gcp_id: response.numId, name: req.body.name }
          admin.database().ref(`devices/${device_id}`).set(firebaseDevice).then(snap => {
            res.status(200).send(response);
          }).catch(err => {
            res.status(400).send("1");
          });
        }).catch(err => {
          res.status(200).send("2");
        });
      }).catch(err => {
        res.status(400).send("3");
      });
    }).catch(err => {
      res.status(400).send("4");
    });
  });

  // get devices for a project
  // params: { team: `${teamID}`, project: `${projectID}` }
  router.get(`/team/:team/project/:project/devices`, (req, res) => {
    admin.database().ref(`projects/${req.params.project}/devices`).once('value').then(snap => {
      let devices = [];
      let promises = [];
      snap.forEach((device) => {
        let p = admin.database().ref(`devices/${device.val()}`).once('value').then(snap => {
          let device = {};
          device[snap.key] = snap.val();
          devices.push(device);
        }).catch(() => {
          res.sendStatus(400);
        });
        promises.push(p);
      });
      Promise.all(promises).then(() => res.status(200).send(devices));
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  // get device
  // params: { team: `${teamID}`, project: `${projectID}`, device: `${deviceID}` }
  router.get(`/team/:team/project/:project/device/:device`, (req, res) => {
    admin.database().ref(`devices/${req.params.device}`).once('value').then(snap => {
      let device = {}
      res.status(200).send(device[snap.key] = snap.val());
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  return router;
}