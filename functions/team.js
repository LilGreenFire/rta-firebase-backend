module.exports = function(express, admin) {
  var router = express.Router();
  let helpers = require("./helpers")(admin);

  // get all teams for user
  router.get('/team', (req, res) => {
    admin.database().ref(`users/${req.user.uid}/teams`).once('value').then(snap => {
      let teams = [];
      let promises = [];
      snap.forEach((teamID) => {
        let p = admin.database().ref(`teams/${teamID.val()}`).once('value').then(snap => {
          let team = {};
          team[snap.key] = snap.val();
          teams.push(team);
        });
        promises.push(p);
      });
      Promise.all(promises).then(() => res.status(200).send(teams));
    });
  });

  // get a single team
  // params {team: `${teamID}`}
  router.get('/team/:team', (req, res) => {
    admin.database().ref(`teams/${req.params.team}`).once('value').then(snap => {
      res.status(200).send(snap.val());
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  // creates team
  // body: { name: `${teamName}`, (optional) decription: `${teamDescription}` }
  router.post('/team', (req, res) => {
    admin.database().ref(`users/${req.user.uid}/username`).once("value").then(snap => {
      let team = {name: req.body.name, members: {}};
      team.members[snap.val()] = "admin";
      team.description = req.body.description || "";
      admin.database().ref('teams').push(team).then(snap => {
        let id = snap.key;
        admin.database().ref(`users/${req.user.uid}/teams`).push(id);
        res.status(200).send(snap.key);
      });
    }).catch(() => {
      res.sendStatus(400);
    })
  });

  // updates description
  // body: {team: `${teamID}`, description: `${teamDescription}`}
  router.put(`/team`, (req, res) => {
    admin.database().ref(`teams/${req.body.team}/description`).set(req.body.description).then(snap => {
      res.status(200).send('Team updated');
    }).catch(() => {
      res.sendStatus(400);
    });
  });

  // remove team
  // params: {team: `${teamID}`}
  router.delete('/team/:team', (req, res) => {
    helpers.isTeamAdmin(req.params.team, req.user.uid).then(username => {
      admin.database().ref(`teams/${req.params.team}/members`).once('value')
        .then(snap => {
          let promises = [];
          let p;

          // delete team from each member
          snap.forEach(member => {
            p = helpers.getUser(member.key).then(user => {
              let uid = Object.keys(user)[0];
              let teams = user[uid].teams;
              for (key in teams) {
                if(teams[key] === req.params.team) {
                  p = admin.database().ref(`users/${uid}/teams/${key}`).remove();
                  promises.push(p);
                }
              }
            }).catch(err => {
              res.status(400).send(err);
            });
            promises.push(p);

            // delete all projects
            p = admin.database().ref(`teams/${req.params.team}/projects`).once('value')
              .then(snap => {
                snap.forEach(project => {
                  p = admin.database().ref(`projects/${project.val()}`).remove();
                  promises.push(p);
                });
              }).catch(() => {
                res.sendStatus(400);
            });
            promises.push(p);

            // delete team itself
            Promise.all(promises).then(() => {
              admin.database().ref(`teams/${req.params.team}`).remove().then(snap => {
                res.status(200).send('deleted');
              }).catch(() => {
                res.sendStatus(400);
              });
            });
          })
        }).catch(() => {
          res.sendStatus(400);
        });
      }).catch(err => {
        res.status(400).send(err);
      });
  });

  // adds member
  // body: {team: `${teamID}`, member: `${username}`, (optional) status: `admin`}
  router.put('/team/addMember', (req, res) => {
    helpers.isTeamAdmin(req.body.team, req.user.uid).then(username => {
      admin.database().ref(`usernames/${req.body.member}`).once('value')
        .then(snap => {
          if(snap.val() === null) {
            res.status(400).send("User doesn't exist");
          } else {
            admin.database().ref(`users/${snap.val()}/teams`).push(req.body.team);
            admin.database().ref(`teams/${req.body.team}/members/${req.body.member}`).set(req.body.status || "member");
            res.status(200).send();
          }
        })
        .catch(() => {
          res.status(400).send("Failed");
        })
      }).catch(err => {
        res.status(400).send(err);
      })
  });

  // remove member
  // params: {team: `${teamID}`, member: `${username}`}
  router.delete('/team/:team/removeMember/:member', (req, res) => {
    helpers.isTeamAdmin(req.params.team, req.user.uid).then(username => {
      admin.database().ref(`usernames/${req.params.member}`).once('value').then(snap => {
        admin.database().ref(`teams/${req.params.team}/members/${req.params.member}`).remove();
        let uid = snap.val();
        admin.database().ref(`users/${uid}/teams`).once('value')
          .then(snap => {
            snap.forEach(team => {
              if(team.val() == req.params.team) {
                admin.database().ref(`users/${uid}/teams/${team.key}`).remove();
                res.status(200).send('deleted');
              }
            });
          })
          .catch(() => {
            res.status(400).send("cannot delete team from user");
          });
      }).catch(() => {
        res.sendStatus(400);
      })
    }).catch(err => {
      res.status(400).send(err);
    });
  });

  // promote member to admin
  // body: {team: `${teamID}`, member: `${username}`}
  router.put('/team/promote', (req, res) => {
    helpers.isTeamAdmin(req.body.team, req.user.uid).then(username => {
      admin.database().ref(`teams/${req.body.team}/members/${req.body.member}`).set("admin").then(snap => {
        res.status(200).send("Promoted");
      }).catch(() => {
        res.sendStatus(400);
      })
    }).catch(err => {
      res.status(400).send(err);
    })
  });

  // demote member to member
  // body: {team: `${teamId}`, member: `${username}`}
  router.put('/team/demote', (req, res) => {
    helpers.isTeamAdmin(req.body.team, req.user.uid).then(username => {
      admin.database().ref(`teams/${req.body.team}/members/${req.body.member}`).set("member").then(snap => {
        res.status(200).send("Demoted");
      }).catch(() => {
        res.status(400).send("Couldn't demote member");
      });
    }).catch(err => {
      res.status(400).send(err);
    })
  });

  router.put

  return router;
}